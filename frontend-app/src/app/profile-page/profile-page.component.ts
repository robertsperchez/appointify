import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Patient } from 'src/models/patient.model';
import { PatientService } from 'src/services/patient.service';
import { Appointment } from 'src/models/appointment.model';
import { AppointmentService } from 'src/services/appointment.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = [
    'appointmentType',
    'appointmentStatus',
    'specialtyType',
    'date',
    'startTime',
    'endTime',
    'viewAppointment',
    'edit',
    'delete'
  ];

  private subscriptionList: Subscription[] = [];
  patientToShow: Patient = new Patient();
  title = 'Patient Info';
  tableTitle = '';
  appointmentsList: Appointment[];
  selectedAppointment: Appointment;

  dataSource = new MatTableDataSource<Appointment>(this.appointmentsList);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private patientService: PatientService,
    private appointmentService: AppointmentService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnDestroy(): void {
    this.subscriptionList.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.subscriptionList.push(
      this.activatedRoute.params.subscribe((param) => {
        if (param.id && parseInt(param.id, 10)) {
          this.setViewPatient(parseInt(param.id, 10));
        }
      })
    );
  }

  updateTable(): void {
    this.dataSource = new MatTableDataSource<Appointment>(this.appointmentsList);
    console.log(this.appointmentsList);
    this.dataSource.paginator = this.paginator;
  }

  private setViewPatient(id: number): void {
    this.patientService.getPatients().subscribe((list) => {
      this.patientToShow = list.find((patient) => patient.id === id);
      this.tableTitle = `${this.patientToShow.firstName}'s appointments:`;
      this.appointmentService.getAppointmentsByPatientId(this.patientToShow.id).subscribe((list: Appointment[]) => {
        this.appointmentsList = list;
        this.updateTable();
      });
    });
  }

  goBackClick() {
    this.router.navigate(['patients']);
  }

  newAppointmentClick() {
    this.router.navigate(['appointments/new']);
  }

  deleteAppointment(id: number, index: number): void {
    if (confirm("Are you sure to delete this appointment?")) {
      this.appointmentService.deleteAppointment(id).subscribe(
        () => {
          this.appointmentsList.splice(index, 1);
          this.updateTable();
        }
      );
    }
  }

  editAppointment(value: Appointment): void {
    this.selectedAppointment = value;
    this.router.navigate(['appointments/edit/patient', this.selectedAppointment.id]);
  }

  viewAppointmentInfo(value: Appointment): void {
    this.selectedAppointment = value;
    this.router.navigate(['appointments/view/patient', this.selectedAppointment.id]);
  }

}

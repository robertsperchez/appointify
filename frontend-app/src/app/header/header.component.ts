import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/services/account.service';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, DoCheck {

  username = this.accountService.username;

  hideLogOut: boolean = this.authService.isAuthenticated();

  constructor(
    private router: Router,
    private accountService: AccountService,
    private authService: AuthService
  ) { }

  ngDoCheck(): void {
    this.hideLogOut = this.authService.isAuthenticated();
    this.username = this.accountService.username;
  }

  ngOnInit(): void {
    this.hideLogOut = this.authService.isAuthenticated();
  }

  goToPatients() {
    this.router.navigate(['patients']);
  }

  goToAppointments() {
    this.router.navigate(['appointments']);
  }

  goToAccounts() {
    this.router.navigate(['accounts']);
  }

  goHome() {
    this.router.navigate(['']);
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  goToContact() {
    this.router.navigate(['contact']);
  }

  onLogOut() {
    this.accountService.removeAuthorization();
    this.goHome();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/services/account.service';
import { CommonService } from 'src/services/common.service';
import { Account, AccountStatus } from 'src/models/account.model';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private accountService: AccountService,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    if (this.accountService.token) {
      this.router.navigate(['patients']);
    }
    this.createForm();
  }

  signup(): void {
    if (!this.form.valid) {
      this.commonService.showSnackBarMessage('Invalid field!');
      return;
    } else {
      const newAccount = new Account(
        null,
        this.form.controls.username.value,
        this.form.controls.password.value,
        AccountStatus.PENDING
      );
      console.log(newAccount);
      this.createAccount(newAccount);
    }
  }

  private createAccount(newAccount: Account): void {
    this.accountService.createAccount(newAccount).subscribe(() => {
      console.log(newAccount); 
      this.commonService.showSnackBarMessage('Account was created successfully! Wait for an admin\'s approval.');
      //this.accountService.saveToken(newAccount.username, newAccount.password);
      //this.accountService.saveAccount(newAccount.username); 
      this.router.navigate(['']); 
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Creation of account has failed!');
    })
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
    })
  }

  get username() { return this.form.get('username'); }

  get password() { return this.form.get('password'); }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  title = 'Where appointments happen';
  paragraph = 'When you need to handle some patients, easily change their information, create some new appointments and see them all in one place, using a friendly user interface, Appointify has you covered. Ease your work with us!';
  text = 'Already have an account?';
  
  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  goToSignup() {
    this.router.navigate(['signup']);
  }

}

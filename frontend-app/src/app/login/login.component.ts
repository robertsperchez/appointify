import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccountService } from 'src/services/account.service';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private commonService: CommonService,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    if (this.accountService.token) {
      this.router.navigate(['patients']);
    }
    this.createForm();
  }

  login(): void {
    const username = this.form.controls.username.value;
    const password = this.form.controls.password.value;
    if (this.form.valid) {
      this.accountService.saveToken(username, password);
      this.router.navigate(['patients']);
      this.accountService.saveAccount(username);
      this.commonService.showSnackBarMessage('Login succesful!');
    } else {
      this.commonService.showSnackBarMessage('Invalid field');
    }
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
    })
  }

  get username() { return this.form.get('username'); }

  get password() { return this.form.get('password'); }

}

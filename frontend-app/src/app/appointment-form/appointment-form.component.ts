import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppointmentType, AppointmentStatus, SpecialtyType, Appointment } from 'src/models/appointment.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppointmentService } from 'src/services/appointment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from 'src/services/patient.service';
import { Patient } from 'src/models/patient.model';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'app-appointment-form',
  templateUrl: './appointment-form.component.html',
  styleUrls: ['./appointment-form.component.scss']
})
export class AppointmentFormComponent implements OnInit, OnDestroy {

  title = 'Insert the appointment information:';
  private subscriptionList: Subscription[] = [];
  appointmentType: string[] = Object.values(AppointmentType);
  appointmentStatus: string[] = Object.values(AppointmentStatus);
  specialtyType: string[] = Object.values(SpecialtyType);
  private appointmentToEdit: Appointment = new Appointment();
  patientsList: Patient[];
  form: FormGroup;
  patientId: number;

  constructor(
    private formBuilder: FormBuilder,
    private appointmentService: AppointmentService,
    private patientService: PatientService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService
  ) { }

  ngOnDestroy(): void {
    this.subscriptionList.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.createForm();
    this.subscriptionList.push(
      this.activatedRoute.params.subscribe((param) => {
        if (param.id && parseInt(param.id, 10)) {
          this.setEditAppointment(parseInt(param.id, 10));
        }
      })
    );
    this.patientService.getPatients().subscribe((list) => this.patientsList = list);
  }

  saveNewAppointment(): void {
    const isValid = this.form.valid;
    const newAppointment: Appointment = {
      ...this.appointmentToEdit,
      ...this.form.getRawValue(),
    };

    if (!isValid) {
      return;
    }

    newAppointment.id > 0 ? this.updateAppointment(newAppointment) : this.createAppointment(newAppointment);
  }

  private createAppointment(newAppointment: Appointment): void {
    this.appointmentService.createAppointment(newAppointment).subscribe(() => {
      this.commonService.showSnackBarMessage('Appointment created succesfully!');
      this.router.navigate(['appointments']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Appointment creation has failed!');
    });
  }

  private updateAppointment(newAppointment: Appointment): void {
    this.appointmentService.updateAppointment(newAppointment).subscribe(() => {
      this.commonService.showSnackBarMessage('Appointment updated succesfully!');
      this.router.navigate(['appointments']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Appointment update has failed!');
    });
  }

  private setEditAppointment(id: number): void {
    this.appointmentService.getAppointments().subscribe((list) => {
      this.appointmentToEdit = list.find((app) => app.id === id);
      this.form.patchValue(this.appointmentToEdit, {
        emitEvent: false,
      });
      this.patientId = this.appointmentToEdit.patientId;
    });
  }

  goBackClick() {
    if (this.router.url == `/appointments/edit/${this.appointmentToEdit.id}`) {
      this.router.navigate(['appointments']);
    } else if (this.router.url == `/appointments/new`) {
      this.router.navigate(['appointments']);
    } else {
      this.router.navigate(['patients/view/', this.patientId]);
    }
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      appointmentType: [AppointmentType.REGULAR],
      appointmentStatus: [AppointmentStatus.CREATED],
      specialtyType: [SpecialtyType.CARDIOLOGY],
      date: [null],
      startTime: new FormControl('', [
        Validators.required,
        Validators.pattern("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
      ]),
      endTime: new FormControl('', [
        Validators.required,
        Validators.pattern("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
      ]),
      description: [null],
      patientId: [null]
    });
  }

  get startTime() { return this.form.get('startTime'); }

  get endTime() { return this.form.get('endTime'); }

}

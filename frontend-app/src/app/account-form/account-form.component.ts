import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AccountService } from 'src/services/account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Account, AccountStatus } from 'src/models/account.model';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnInit {

  title = 'Insert the account\'s information:';
  private subscriptionList: Subscription[] = [];
  accountStatus: string[] = Object.values(AccountStatus);
  private accountToEdit: Account = new Account();
  form: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService
  ) { }

  ngOnDestroy(): void {
    this.subscriptionList.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.createForm();
    this.subscriptionList.push(
      this.activatedRoute.params.subscribe((param) => {
        if (param.id && parseInt(param.id, 10)) {
          this.setEditAccount(parseInt(param.id, 10));
        }
      })
    );
  }

  saveNewAccount() {
    const isFormValid = this.form.valid;
    console.log(this.form.getRawValue());
    const newAccount: Account = {
      ...this.accountToEdit,
      ...this.form.getRawValue()
    }
    
    if(!isFormValid) {
      return;
    }

    newAccount.id > 0 ?  this.updateAccount(newAccount) : this.createAccount(newAccount);
  }

  private createAccount(newAccount: Account): void {
    this.accountService.createAccount(newAccount).subscribe(() => {
      this.commonService.showSnackBarMessage('Account created succesfully!');
      this.router.navigate(['accounts']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Account creation has failed!');
    });
  }

  private updateAccount(newAccount: Account): void {
    this.accountService.updateAccount(newAccount).subscribe(() => {
      this.commonService.showSnackBarMessage('Account updated succesfully!');
      this.router.navigate(['accounts']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Account update has failed!');
    });
  }

  private setEditAccount(id: number): void {
    this.accountService.getAccounts().subscribe((list) => {
      this.accountToEdit = list.find((account) => account.id === id);
      this.form.patchValue(this.accountToEdit, {
        emitEvent: false,
      });
    });
  }
 
  goBackClick() {
    this.router.navigate(['accounts']);
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
      accountStatus: [AccountStatus.PENDING],
    });
  }

  get username() { return this.form.get('username'); }

  get password() { return this.form.get('password'); }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { AppointmentFormComponent } from './appointment-form/appointment-form.component';
import { AppointmentInfoComponent } from './appointment-info/appointment-info.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'patients', component: PatientListComponent, canActivate: [AuthGuard] },
  { path: 'patients/new', component: PatientFormComponent, canActivate: [AuthGuard] },
  { path: 'patients/edit/:id', component: PatientFormComponent, canActivate: [AuthGuard] },
  { path: 'patients/view/:id', component: ProfilePageComponent, canActivate: [AuthGuard] },
  { path: 'appointments', component: AppointmentListComponent, canActivate: [AuthGuard] },
  { path: 'appointments/new', component: AppointmentFormComponent, canActivate: [AuthGuard] },
  { path: 'appointments/edit/:id', component: AppointmentFormComponent, canActivate: [AuthGuard] },
  { path: 'appointments/view/:id', component: AppointmentInfoComponent, canActivate: [AuthGuard] },
  { path: 'appointments/view/patient/:id', component: AppointmentInfoComponent, canActivate: [AuthGuard] },
  { path: 'appointments/edit/patient/:id', component: AppointmentFormComponent, canActivate: [AuthGuard] },
  { path: 'accounts', component: AccountListComponent, canActivate: [AuthGuard] },
  { path: 'accounts/new', component: AccountFormComponent, canActivate: [AuthGuard] },
  { path: 'accounts/edit/:id', component: AccountFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } 

import { Component, OnInit, ViewChild } from '@angular/core';
import { Appointment } from 'src/models/appointment.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AppointmentService } from 'src/services/appointment.service';
import { Router } from '@angular/router';
import { PatientService } from 'src/services/patient.service';
import { Patient } from 'src/models/patient.model';
import { AppointmentInfoComponent } from '../appointment-info/appointment-info.component';

@Component({
  selector: 'appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss']
})
export class AppointmentListComponent implements OnInit {

  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'appointmentType',
    'appointmentStatus',
    'specialtyType',
    'date',
    'startTime',
    'endTime',
    'viewAppointment',
    'edit',
    'delete'
  ];

  title = 'Here is a list of all the appointments:';
  selectedAppointment: Appointment;
  appointmentsList: Appointment[];

  dataSource = new MatTableDataSource<Appointment>(this.appointmentsList);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private appointmentService: AppointmentService,
    private router: Router,
    private patientService: PatientService
  ) { }

  ngOnInit(): void {
    this.getAppointmentList();
  }

  getAppointmentList(): void {
    this.appointmentService.getAppointments().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  sortAppointmentsByDateAndHourAsc(): void {
    this.appointmentService.sortAppointmentsByDateAndHourAsc().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  sortAppointmentsByDateDesc(): void {
    this.appointmentService.sortAppointmentsByDateDesc().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  sortAppointmentsByType(): void {
    this.appointmentService.sortAppointmentsByType().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  sortAppointmentsByStatus(): void {
    this.appointmentService.sortAppointmentsByStatus().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  sortAppointmentsBySpecialty(): void {
    this.appointmentService.sortAppointmentsBySpecialty().subscribe((list: Appointment[]) => {
      this.appointmentsList = list;
      this.appointmentsList.forEach(appointment => {
        this.patientService.getPatientById(appointment.patientId).subscribe((patient: Patient) => {
          appointment.patient = patient;
        });
      });
      this.updateTable();
    });
  }

  updateTable(): void {
    this.dataSource = new MatTableDataSource<Appointment>(this.appointmentsList);
    console.log(this.appointmentsList);
    this.dataSource.paginator = this.paginator;
  }

  newAppointmentClick() {
    this.router.navigate(['appointments/new']);
  }

  deleteAppointment(id: number, index: number): void {
    if (confirm("Are you sure to delete this appointment?")) {
      this.appointmentService.deleteAppointment(id).subscribe(
        () => {
          this.appointmentsList.splice(index, 1);
          this.updateTable();
        }
      );
    }
  }

  editAppointment(value: Appointment): void {
    this.selectedAppointment = value;
    this.router.navigate(['appointments/edit/', this.selectedAppointment.id]);
  }

  viewAppointmentInfo(value: Appointment): void {
    this.selectedAppointment = value;
    this.router.navigate(['appointments/view/', this.selectedAppointment.id]);
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SexType, Patient } from 'src/models/patient.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PatientService } from 'src/services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { CommonService } from 'src/services/common.service';
import { CustomValidationService } from 'src/services/custom-validation.service';

@Component({
  selector: 'patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.scss']
})
export class PatientFormComponent implements OnInit, OnDestroy {

  title = 'Insert the patient\'s information:';
  private subscriptionList: Subscription[] = [];
  sexType: string[] = Object.values(SexType);
  private patientToEdit: Patient = new Patient();
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private patientService: PatientService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService,
    private customValidationService: CustomValidationService
  ) { }

  ngOnDestroy(): void {
    this.subscriptionList.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.createForm();
    this.subscriptionList.push(
      this.activatedRoute.params.subscribe((param) => {
        if (param.id && parseInt(param.id, 10)) {
          this.setEditPatient(parseInt(param.id, 10));
        }
      })
    );
  }

  saveNewPatient() {
    const isFormValid = this.form.valid;
    console.log(this.form.getRawValue());
    const newPatient: Patient = {
      ...this.patientToEdit,
      ...this.form.getRawValue()
    }

    if (!isFormValid) {
      return;
    }

    newPatient.id > 0 ? this.updatePatient(newPatient) : this.createPatient(newPatient);
  }

  private createPatient(newPatient: Patient): void {
    this.patientService.createPatient(newPatient).subscribe(() => {
      this.commonService.showSnackBarMessage('Patient created succesfully!');
      this.router.navigate(['patients']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Patient creation has failed!');
    });
  }

  private updatePatient(newPatient: Patient): void {
    this.patientService.updatePatient(newPatient).subscribe(() => {
      this.commonService.showSnackBarMessage('Patient updated succesfully!');
      this.router.navigate(['patients']);
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Patient update has failed!');
    });
  }

  private setEditPatient(id: number): void {
    this.patientService.getPatients().subscribe((list) => {
      this.patientToEdit = list.find((patient) => patient.id === id);
      this.form.patchValue(this.patientToEdit, {
        emitEvent: false,
      });
    });
  }

  goBackClick() {
    this.router.navigate(['patients']);
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("^[A-Za-z]+[,.'-]*[A-Za-z]$")
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern("^[A-Za-z]+[,.'-]*[A-Za-z]$")
      ]),
      birthDate: [null],
      personalNumericCode: new FormControl('', [
        Validators.required,
        Validators.minLength(13),
        Validators.maxLength(13),
        Validators.pattern("^[0-9]*$")
      ]),
      sexType: [SexType.UNDEFINED],
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      phoneNumber: [null],
      address: [null],
      city: [null],
      country: [null]
    });
  } 

  get firstName() { return this.form.get('firstName'); }

  get lastName() { return this.form.get('lastName'); }

  get birthDate() { return this.form.get('birthDate'); }

  get personalNumericCode() { return this.form.get('personalNumericCode'); }

  get email() { return this.form.get('email'); }

  get phoneNumber() { return this.form.get('phoneNumber'); }

}

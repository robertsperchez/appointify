import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientService } from '../services/patient.service';
import { AccountService } from '../services/account.service';
import { CommonService } from '../services/common.service';
import { BaseService } from '../services/base.service';
import { ContactEmailService } from '../services/contact-email.service';
import { AppointmentService } from '../services/appointment.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BannerComponent } from './banner/banner.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { MatCardModule } from '@angular/material/card';
import { AppointmentFormComponent } from './appointment-form/appointment-form.component';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { AppointmentInfoComponent } from './appointment-info/appointment-info.component';
import { BannerAppointmentsComponent } from './banner-appointments/banner-appointments.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { HomepageComponent } from './homepage/homepage.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TokenInterceptor } from '../interceptors/token.interceptor';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthService } from '../services/auth.service';
import { AuthGuardService } from '../services/auth-guard.service';
import { BannerUserComponent } from './banner-user/banner-user.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { CustomValidationService } from '../services/custom-validation.service';
import { ContactComponent } from './contact/contact.component';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PatientListComponent,
    BannerComponent,
    PatientFormComponent,
    ProfilePageComponent,
    AppointmentFormComponent,
    AppointmentListComponent,
    AppointmentInfoComponent,
    BannerAppointmentsComponent,
    HomepageComponent,
    LoginComponent,
    SignupComponent,
    BannerUserComponent,
    AccountListComponent,
    AccountFormComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    HttpClientModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatPaginatorModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatCardModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatSnackBarModule,
    GoogleMapsModule
  ],
  providers: [
    PatientService,
    BaseService,
    AppointmentService,
    AccountService,
    CommonService,
    AuthService,
    AuthGuardService,
    ContactEmailService,
    CustomValidationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

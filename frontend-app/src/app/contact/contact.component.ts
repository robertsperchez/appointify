import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from
  '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/services/common.service';
import { ContactEmail } from 'src/models/contact-email.model';
import { ContactEmailService } from 'src/services/contact-email.service';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private contactEmailService: ContactEmailService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  sendNewEmail() {
    const isFormValid = this.form.valid;
    console.log(this.form.getRawValue());
    const newEmail: ContactEmail = this.form.getRawValue();

    if (!isFormValid) {
      this.commonService.showSnackBarMessage('Invalid fields!');
      return;
    }

    this.sendEmail(newEmail);
  }

  private sendEmail(newEmail: ContactEmail): void {
    this.contactEmailService.sendEmail(newEmail).subscribe(() => {
      this.commonService.showSnackBarMessage('Email sent succesfully! Someone from our company will contact you shortly.');
    }, (err) => {
      console.error(err);
      this.commonService.showSnackBarMessage('Email sending failed!');
    });
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      phoneNumber: new FormControl('', [
        Validators.pattern("[0-9]+")
      ]),
      company: [null],
      message: new FormControl('', [
        Validators.required,
        Validators.maxLength(512)
      ])
    });
  }

  get name() { return this.form.get('name'); }

  get email() { return this.form.get('email'); }

  get phoneNumber() { return this.form.get('phoneNumber'); }

  get message() { return this.form.get('message'); }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Account } from 'src/models/account.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AccountService } from 'src/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  displayedColumns: string[] = [
    'username',
    'accountStatus',
    'edit',
    'delete'
  ];

  title = 'Here is a list of all the accounts:';
  selectedAccount: Account;
  accountsList: Account[];

  dataSource = new MatTableDataSource<Account>(this.accountsList);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAccountList();
  }

  getAccountList(): void {
    this.accountService.getAccounts().subscribe((list: Account[]) => {
      console.log(list)
      this.accountsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  updateTable(): void {
    this.dataSource = new MatTableDataSource<Account>(this.accountsList);
    this.dataSource.paginator = this.paginator;
  }

  newAccountClick() {
    this.router.navigate(['accounts/new']);
  }

  deleteAccount(id: number, index: number): void {
    if (confirm("Are you sure to delete this account?")) {
      this.accountService.deleteAccount(id).subscribe(
        () => {
          this.accountsList.splice(index, 1);
          this.updateTable();
        }
      );
    }
  }

  editAccount(value: Account): void {
    this.selectedAccount = value;
    this.router.navigate(['accounts/edit/', this.selectedAccount.id]);
  }
}

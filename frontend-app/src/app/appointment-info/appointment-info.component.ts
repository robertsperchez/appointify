import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { PatientService } from 'src/services/patient.service';
import { Subscription } from 'rxjs';
import { Appointment } from 'src/models/appointment.model';
import { AppointmentService } from 'src/services/appointment.service';
import { Patient } from 'src/models/patient.model';

@Component({
  selector: 'appointment-info',
  templateUrl: './appointment-info.component.html',
  styleUrls: ['./appointment-info.component.scss']
})
export class AppointmentInfoComponent implements OnInit, OnDestroy {

  private subscriptionList: Subscription[] = [];
  appointmentToShow: Appointment = new Appointment();
  title = 'Appointment Info';
  patientId: number;

  constructor(
    private appointmentService: AppointmentService,
    private patientService: PatientService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnDestroy(): void {
    this.subscriptionList.forEach((sub) => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.subscriptionList.push(
      this.activatedRoute.params.subscribe((param) => {
        if (param.id && parseInt(param.id, 10)) {
          this.setViewAppointment(parseInt(param.id, 10));
        }
      })
    );
  }

  private setViewAppointment(id: number): void {
    this.appointmentService.getAppointments().subscribe((list) => {
      this.appointmentToShow = list.find((appointment) => appointment.id === id);
      this.patientService.getPatientById(this.appointmentToShow.patientId).subscribe((patient: Patient) => {
        this.appointmentToShow.patient = patient;
      })
      this.patientId = this.appointmentToShow.patientId;
    });

  }

  goBackClick() {
    if (this.router.url == `/appointments/view/${this.appointmentToShow.id}`) {
      this.router.navigate(['appointments']);
    } else {
      this.router.navigate(['patients/view/', this.patientId]);
    }
  }

}

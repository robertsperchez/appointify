import { Component, OnInit, ViewChild } from '@angular/core';
import { Patient } from '../../models/patient.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PatientService } from 'src/services/patient.service';

@Component({
  selector: 'patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'email',
    'birthDate',
    'sexType',
    'viewProfileAppointments',
    'edit',
    'delete'
  ];

  title = 'Here is a list of all the patients:';
  selectedPatient: Patient;
  patientsList: Patient[];

  dataSource = new MatTableDataSource<Patient>(this.patientsList);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private patientService: PatientService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getPatientList();
  }

  getPatientList(): void {
    this.patientService.getPatients().subscribe((list: Patient[]) => {
      console.log(list)
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  sortPatientsByNameAsc(): void {
    this.patientService.sortPatientsByNameAsc().subscribe((list: Patient[]) => {
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  sortPatientsByNameDesc(): void {
    this.patientService.sortPatientsByNameDesc().subscribe((list: Patient[]) => {
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  sortPatientsByBirthdateAsc(): void {
    this.patientService.sortPatientsByBirthdateAsc().subscribe((list: Patient[]) => {
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  sortPatientsByBirthdateDesc(): void {
    this.patientService.sortPatientsByBirthdateDesc().subscribe((list: Patient[]) => {
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  sortPatientsBySexType(): void {
    this.patientService.sortPatientsBySexType().subscribe((list: Patient[]) => {
      this.patientsList = list;
      this.updateTable();
    }, (error) => { console.error(error) });
  }

  updateTable(): void {
    this.dataSource = new MatTableDataSource<Patient>(this.patientsList);
    this.dataSource.paginator = this.paginator;
  }

  newPatientClick() {
    this.router.navigate(['patients/new']);
  }

  deletePatient(id: number, index: number): void {
    if (confirm("Are you sure to delete this patient?")) {
      this.patientService.deletePatient(id).subscribe(
        () => {
          this.patientsList.splice(index, 1);
          this.updateTable();
        }
      );
    }
  }

  editPatient(value: Patient): void {
    this.selectedPatient = value;
    this.router.navigate(['patients/edit/', this.selectedPatient.id]);
  }

  viewProfileAppointments(value: Patient): void {
    this.selectedPatient = value;
    this.router.navigate(['patients/view/', this.selectedPatient.id]);
  }

}

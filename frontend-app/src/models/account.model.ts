export class Account {
    constructor (
        public id: number = 0,
        public username?: string,
        public password?: string,
        public accountStatus: AccountStatus = AccountStatus.PENDING
    ) {}
}

export enum AccountStatus {
    APPROVED = 'APPROVED',
    PENDING = 'PENDING'
}
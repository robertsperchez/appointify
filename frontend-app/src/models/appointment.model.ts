import { Patient } from './patient.model'

export class Appointment {
    constructor(
        public id: number = 0,
        public appointmentType: AppointmentType = AppointmentType.REGULAR,
        public appointmentStatus: AppointmentStatus = AppointmentStatus.CREATED,
        public specialtyType: SpecialtyType = SpecialtyType.CARDIOLOGY,
        public date?: Date,
        public startTime?: string,
        public endTime?: string,
        public description?: string,
        public patient?: Patient,
        public patientId?: number
    ) { }
}

export enum AppointmentStatus {
    CREATED = 'CREATED',
    PLANNED = 'PLANNED',
    CONFIRMED = 'CONFIRMED',
    CLOSED = 'CLOSED',
    CANCELLED = 'CANCELLED'
}

export enum AppointmentType {
    REGULAR = 'REGULAR',
    HOLIDAY = 'HOLIDAY',
    VACATION = 'VACATION',
    GROUP = 'GROUP'
}

export enum SpecialtyType {
    CARDIOLOGY = 'CARDIOLOGY',
    DERMATHOLOGY = 'DERMATHOLOGY',
    SURGERY = 'SURGERY',
    ECOGRAPHY = 'ECOGRAPHY',
    EPIDEMIOLOGY = 'EPIDEMIOLOGY',
    PHYSIOTHERAPY = 'PHYSIOTHERAPY',
    KINETOTHERAPY = 'KINETOTHERAPY',
    NEUROLOGY = 'NEUROLOGY',
    OPHTALMOLOGY = 'OPHTALMOLOGY',
    ONCOLOGY = 'ONCOLOGY',
    IMUNOLOGY = 'IMUNOLOGY',
    RMN = 'RMN',
    UROLOGY = 'UROLOGY',
    CT = 'CT',
    MAMOGRAPHY = 'MAMOGRAPHY',
    HEMATOLOGY = 'HEMATOLOGY',
    ORL = 'ORL',
    PSYCHIATRY = 'PSYCHIATRY',
    PSYCHOLOGY = 'PSYCHOLOGY'
}
export class Patient {
    constructor(
        public id: number = 0,
        public firstName?: string,
        public lastName?: string,
        public birthDate?: Date,
        public personalNumericCode?: string,
        public sexType: SexType = SexType.UNDEFINED,
        public city?: string,
        public country?: string,
        public email?: string,
        public phoneNumber?: string,
        public address?: string
    ) { }
}

export enum SexType {
    MALE = 'MALE',
    FEMALE = 'FEMALE',
    UNDEFINED = 'UNDEFINED',
}
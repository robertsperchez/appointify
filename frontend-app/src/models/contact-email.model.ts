export class ContactEmail {
    constructor(
        public company?: string,
        public email?: string,
        public message?: string,
        public name?: string,
        public phoneNumber?: string
    ) { }
}
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AccountService } from 'src/services/account.service';
import { CommonService } from 'src/services/common.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private accountService: AccountService,
        private commonService: CommonService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       const request = req.clone({
            setHeaders: { authorization: this.accountService.token }
        })
        return next.handle(request).pipe(
            tap((event: HttpEvent<any>) => {
                console.log(event);
            }, (err) => { 
                if (err.status === 401) {
                    this.commonService.loginFailed();
                }
            })
        );
    }

}
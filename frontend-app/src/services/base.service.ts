import { environment } from "../environments/environment.prod";

export class BaseService {
    public API = environment.API_URL;
}
import { Injectable } from '@angular/core';

@Injectable()
export class CustomValidationService {

    validateBirthDate(birthDate: Date) {
        return (birthDate.getDate > Date.now);
    }

}
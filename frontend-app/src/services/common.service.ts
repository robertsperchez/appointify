import { Injectable } from '@angular/core';
import { AccountService } from './account.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class CommonService {

  constructor(
    private snackBar: MatSnackBar,
    private accountService: AccountService,
    private router: Router
  ) { }

  showSnackBarMessage(message: string): void {
    this.snackBar.open(message, 'close', { duration: 3000 });
  }

  loginFailed() {
    this.accountService.loggedInAccount = undefined;
    this.accountService.removeAuthorization();
    this.router.navigate(['']);
    this.showSnackBarMessage('Please log in!');
  }
}
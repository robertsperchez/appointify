import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

    constructor() { }

    public isAuthenticated(): boolean {
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        if (token && username) {
            return true;
        }
        else {
            return false;
        }
    }
}
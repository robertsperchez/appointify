import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { ContactEmail } from 'src/models/contact-email.model';
import { Observable } from 'rxjs';

@Injectable()
export class ContactEmailService extends BaseService {

    constructor(
        private httpClient: HttpClient
    ) {
        super();
    }

    sendEmail(object: ContactEmail): Observable<ContactEmail> {
        const url = `${this.API}/sendemail`;
        return this.httpClient.post(url, object) as Observable<ContactEmail>;
    }

} 
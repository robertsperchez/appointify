import { BaseService } from "./base.service";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient } from 'src/models/patient.model';

@Injectable()
export class PatientService extends BaseService {

    constructor(
        private httpClient: HttpClient
    ) {
        super();
    }

    getPatients(): Observable<Patient[]> {
        const url = `${this.API}/patient`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    sortPatientsByNameAsc(): Observable<Patient[]> {
        const url = `${this.API}/patient/sortByNameAsc`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    sortPatientsByNameDesc(): Observable<Patient[]> {
        const url = `${this.API}/patient/sortByNameDesc`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    sortPatientsByBirthdateAsc(): Observable<Patient[]> {
        const url = `${this.API}/patient/sortByBirthdateAsc`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    sortPatientsByBirthdateDesc(): Observable<Patient[]> {
        const url = `${this.API}/patient/sortByBirthdateDesc`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    sortPatientsBySexType(): Observable<Patient[]> {
        const url = `${this.API}/patient/sortBySexType`;
        return this.httpClient.get(url) as Observable<Patient[]>;
    }

    getPatientById(id: number): Observable<Patient> {
        const url = `${this.API}/patient/${id}`;
        return this.httpClient.get(url) as Observable<Patient>;
    }

    createPatient(object: Patient): Observable<Patient> {
        const url = `${this.API}/patient/create`;
        return this.httpClient.post(url, object) as Observable<Patient>;
    }

    updatePatient(object: Patient): Observable<Patient> {
        const url = `${this.API}/patient/update`;
        return this.httpClient.put(url, object) as Observable<Patient>;
    }

    deletePatient(id: number): Observable<null> {
        const url = `${this.API}/patient/${id}/delete`;
        return this.httpClient.delete(url) as Observable<null>;
    }

}
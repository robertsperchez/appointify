import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Account, AccountStatus } from 'src/models/account.model';

@Injectable()
export class AccountService extends BaseService {

    get token(): string {
        return localStorage.getItem('token') || '';
    }

    set token(value: string) {
        localStorage.setItem('token', value);
    }

    get username(): string {
        return localStorage.getItem('username') || 'unknown';
    }

    set username(value: string) {
        localStorage.setItem('username', value);
    }

    loggedInAccount: BehaviorSubject<string> = new BehaviorSubject(this.username);

    constructor(
        private httpClient: HttpClient
    ) {
        super();
    }

    removeAuthorization() {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
    }

    saveAccount(username: string) {
        localStorage.setItem('username', username);
        this.loggedInAccount.next(username);
    }

    saveToken(username: string, password: string) {
        const generatedToken = `Basic ${btoa(username + ':' + password)}`;
        localStorage.setItem('token', generatedToken);
        this.token = generatedToken;
    }

    getAccounts(): Observable<Account[]> {
        const url = `${this.API}/account`;
        return this.httpClient.get(url) as Observable<Account[]>;
    }

    getAccountByUsername(username: string): Observable<Account> {
        const url = `${this.API}/account/username/${username}`;
        return this.httpClient.get(url) as Observable<Account>;
    }

    createAccount(object: Account): Observable<Account> {
        const url = `${this.API}/account/create`;
        return this.httpClient.post(url, object) as Observable<Account>;
    }

    updateAccount(object: Account): Observable<Account> {
        const url = `${this.API}/account/update`;
        return this.httpClient.put(url, object) as Observable<Account>;
    }

    deleteAccount(id: number): Observable<null> {
        const url = `${this.API}/account/${id}/delete`;
        return this.httpClient.delete(url) as Observable<null>;
    }
}
import { BaseService } from "./base.service";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Appointment } from 'src/models/appointment.model';

@Injectable()
export class AppointmentService extends BaseService {

    constructor(
        private httpClient: HttpClient
    ) {
        super();
    }

    getAppointments(): Observable<Appointment[]> {
        const url = `${this.API}/appointment`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    sortAppointmentsByDateAndHourAsc(): Observable<Appointment[]> {
        const url = `${this.API}/appointment/sortByDateAsc`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    sortAppointmentsByDateDesc(): Observable<Appointment[]> {
        const url = `${this.API}/appointment/sortByDateDesc`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    sortAppointmentsByType(): Observable<Appointment[]> {
        const url = `${this.API}/appointment/sortByType`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    sortAppointmentsByStatus(): Observable<Appointment[]> {
        const url = `${this.API}/appointment/sortByStatus`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    sortAppointmentsBySpecialty(): Observable<Appointment[]> {
        const url = `${this.API}/appointment/sortBySpecialty`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    getAppointmentsByPatientId(patientId: number): Observable<Appointment[]> {
        const url = `${this.API}/appointment/patient/${patientId}`;
        return this.httpClient.get(url) as Observable<Appointment[]>;
    }

    createAppointment(object: Appointment): Observable<Appointment> {
        const url = `${this.API}/appointment/create`;
        return this.httpClient.post(url, object) as Observable<Appointment>;
    }

    updateAppointment(object: Appointment): Observable<Appointment> {
        const url = `${this.API}/appointment/update`;
        return this.httpClient.put(url, object) as Observable<Appointment>;
    }

    deleteAppointment(id: number): Observable<null> {
        const url = `${this.API}/appointment/${id}/delete`;
        return this.httpClient.delete(url) as Observable<null>;
    }

}
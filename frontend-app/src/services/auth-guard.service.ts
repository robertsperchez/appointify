import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { AccountService } from './account.service';
import { AccountStatus } from 'src/models/account.model';
import { CommonService } from './common.service';
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    public auth: AuthService,
    public accountService: AccountService,
    public commonService: CommonService,
    public router: Router
  ) { }

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['']);
      return false;
    }
    else {
      const username = localStorage.getItem('username');
      this.accountService.getAccountByUsername(username).subscribe((value) => {
        if (value.accountStatus == AccountStatus.PENDING) {
          this.commonService.showSnackBarMessage("You are logged in, but the account is not yet approved!");
          this.router.navigate(['']);
          return false;
        }
      });
    }
    return true;
  }

}
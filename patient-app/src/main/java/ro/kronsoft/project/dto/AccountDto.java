package ro.kronsoft.project.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ro.kronsoft.project.entities.enums.AccountStatus;
import ro.kronsoft.project.validations.AccountValidation;

@AccountValidation
public class AccountDto extends BaseDto {

	private static final long serialVersionUID = 4608790981594822442L;

	@NotNull
	@Size(min = 4, max = 32)
	private String username;

	@Size(min = 8, max = 64)
	private String password;

	@NotNull
	private AccountStatus authority;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AccountStatus getAccountStatus() {
		return authority;
	}

	public void setAccountStatus(AccountStatus authority) {
		this.authority = authority;
	}

	public AccountDto(@NotNull @Size(min = 0, max = 32) String username,
			@Size(min = 0, max = 64) String password, @NotNull AccountStatus authority) {
		super();
		this.username = username;
		this.password = password;
		this.authority = authority;
	}

	public AccountDto() {
		super();
	}

	@Override
	public String toString() {
		return "AccountDto [username=" + username + ", password=" + password + ", accountStatus=" + authority + "]";
	}

}

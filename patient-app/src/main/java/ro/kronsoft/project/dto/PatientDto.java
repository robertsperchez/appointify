package ro.kronsoft.project.dto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ro.kronsoft.project.entities.enums.SexTypeEnum;
import ro.kronsoft.project.validations.PatientValidation;

@PatientValidation
public class PatientDto extends BaseDto {

	private static final long serialVersionUID = -4877880881295426678L;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String firstName;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String lastName;

	@NotNull
	private LocalDate birthDate;

	@NotNull
	@NotBlank
	@Size(min = 13, max = 13)
	@Pattern(regexp = "^[0-9]*$")
	private String personalNumericCode;

	@NotNull
	private SexTypeEnum sexType;

	@Size(min = 0, max = 64)
	private String city;

	@Size(min = 0, max = 64)
	private String country;

	@NotNull
	@NotBlank
	@Size(min = 0, max = 64)
	@Email
	private String email;

	@NotNull
	@NotBlank
	@Size(min = 0, max = 32)
	@Pattern(regexp = "[0-9]+")
	private String phoneNumber;

	@Size(min = 0, max = 128)
	private String address;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getPersonalNumericCode() {
		return personalNumericCode;
	}

	public void setPersonalNumericCode(String personalNumericCode) {
		this.personalNumericCode = personalNumericCode;
	}

	public SexTypeEnum getSexType() {
		return sexType;
	}

	public void setSexType(SexTypeEnum sexType) {
		this.sexType = sexType;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "PatientDto [firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate
				+ ", personalNumericCode=" + personalNumericCode + ", sexType=" + sexType + ", city=" + city
				+ ", country=" + country + ", email=" + email + ", phoneNumber=" + phoneNumber + ", address=" + address
				+ "]";
	}

	public PatientDto(@NotNull @Size(min = 0, max = 64) String firstName,
			@NotNull @Size(min = 0, max = 64) String lastName, @NotNull LocalDate birthDate,
			@NotNull @Size(min = 0, max = 32) String personalNumericCode, @NotNull SexTypeEnum sexType,
			@Size(min = 0, max = 64) String city, @Size(min = 0, max = 64) String country,
			@NotNull @Size(min = 0, max = 64) @Email String email, @NotNull @Size(min = 0, max = 32) String phoneNumber,
			@Size(min = 0, max = 128) String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.personalNumericCode = personalNumericCode;
		this.sexType = sexType;
		this.city = city;
		this.country = country;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public PatientDto() {
		super();
	}

}

package ro.kronsoft.project.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ContactEmailDto implements Serializable {

	private static final long serialVersionUID = 6527702459256972082L;

	@NotNull
	@Size(min = 4, max = 32)
	private String name;

	@NotNull
	@Size(min = 0, max = 64)
	@Email
	private String email;

	@Size(min = 10, max = 10)
	@Pattern(regexp = "[0-9]+")
	private String phoneNumber;

	@Size(min = 0, max = 64)
	private String company;

	@NotBlank
	@NotNull
	@Size(min = 0, max = 512)
	private String message;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ContactEmailDto [name=" + name + ", email=" + email + ", phoneNumber=" + phoneNumber + ", company="
				+ company + ", message=" + message + "]";
	}

	public ContactEmailDto(@NotNull @Size(min = 4, max = 32) String name,
			@NotNull @Size(min = 0, max = 64) @Email String email,
			@Size(min = 10, max = 10) @Pattern(regexp = "[0-9]+") String phoneNumber,
			@Size(min = 0, max = 64) String company, @NotBlank @NotNull @Size(min = 0, max = 512) String message) {
		super();
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.company = company;
		this.message = message;
	}

	public ContactEmailDto() {
		super();
	}

}
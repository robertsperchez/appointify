package ro.kronsoft.project.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import ro.kronsoft.project.entities.enums.AppointmentStatus;
import ro.kronsoft.project.entities.enums.AppointmentType;
import ro.kronsoft.project.entities.enums.SpecialtyType;
import ro.kronsoft.project.validations.AppointmentValidation;

@AppointmentValidation
public class AppointmentDto extends BaseDto {

	private static final long serialVersionUID = -4425652766816094125L;

	@NotNull
	private AppointmentType appointmentType;

	@NotNull
	private AppointmentStatus appointmentStatus;

	@NotNull
	private SpecialtyType specialtyType;

	@NotNull
	private LocalDate date;

	@NotNull
	@JsonFormat(pattern="HH:mm")
	private LocalTime startTime;

	@NotNull
	@JsonFormat(pattern="HH:mm")
	private LocalTime endTime;

	@Size(min = 0, max = 512)
	private String description;

	@NotNull
	@Positive
	private Long patientId;

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public AppointmentDto(@NotNull AppointmentType appointmentType, @NotNull AppointmentStatus appointmentStatus,
			@NotNull SpecialtyType specialtyType, @NotNull LocalDate date, @NotNull LocalTime startTime,
			@NotNull LocalTime endTime, @Size(min = 0, max = 512) String description, @NotNull @Positive Long patientId) {
		super();
		this.appointmentType = appointmentType;
		this.appointmentStatus = appointmentStatus;
		this.specialtyType = specialtyType;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.patientId = patientId;
	}

	public AppointmentDto() {
		super();
	}

	@Override
	public String toString() {
		return "AppointmentDto [appointmentType=" + appointmentType + ", appointmentStatus=" + appointmentStatus
				+ ", specialtyType=" + specialtyType + ", date=" + date + ", startTime=" + startTime + ", endTime="
				+ endTime + ", description=" + description + ", patientId=" + patientId + "]";
	}

}

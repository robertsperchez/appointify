package ro.kronsoft.project.dto;

import java.io.Serializable;

public class BaseDto implements Serializable{

	private static final long serialVersionUID = -6840888134441683285L;
	
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

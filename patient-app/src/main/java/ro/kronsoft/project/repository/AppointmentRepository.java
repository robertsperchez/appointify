package ro.kronsoft.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.kronsoft.project.entities.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long>{

	Appointment getById(Long id);
	
	List<Appointment> getByPatientId(Long id);
	
	@Query(value = "SELECT * FROM appointment ORDER BY date, start_time ASC", nativeQuery = true)
	List<Appointment> sortByDateAndHourAsc();
	
	@Query(value = "SELECT * FROM appointment ORDER BY date DESC", nativeQuery = true)
	List<Appointment> sortByDateDesc();
	
	@Query(value = "SELECT * FROM appointment ORDER BY type ASC", nativeQuery = true)
	List<Appointment> sortByType();
	
	@Query(value = "SELECT * FROM appointment ORDER BY specialty ASC", nativeQuery = true)
	List<Appointment> sortBySpecialty();
	
	@Query(value = "SELECT * FROM appointment ORDER BY status ASC", nativeQuery = true)
	List<Appointment> sortByStatus();
	
}

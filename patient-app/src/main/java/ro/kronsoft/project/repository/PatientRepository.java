package ro.kronsoft.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ro.kronsoft.project.entities.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

	Patient getById(Long id);

	boolean existsByEmail(String email);

	@Query(value = "SELECT EXISTS(SELECT * FROM patient WHERE id != :id AND email = :email)", nativeQuery = true)
	boolean existsByEmailNotId(@Param("email") String email, @Param("id") Long id);
	
	@Query(value = "SELECT * FROM patient ORDER BY last_name, first_name ASC", nativeQuery = true)
	List<Patient> sortByNameAsc();
	
	@Query(value = "SELECT * FROM patient ORDER BY last_name DESC, first_name ASC", nativeQuery = true)
	List<Patient> sortByNameDesc();
	
	@Query(value = "SELECT * FROM patient ORDER BY birthdate ASC", nativeQuery = true)
	List<Patient> sortByBirthdateAsc();
	
	@Query(value = "SELECT * FROM patient ORDER BY birthdate DESC", nativeQuery = true)
	List<Patient> sortByBirthdateDesc();
	
	@Query(value = "SELECT * FROM patient ORDER BY sex", nativeQuery = true)
	List<Patient> sortBySexType();
}

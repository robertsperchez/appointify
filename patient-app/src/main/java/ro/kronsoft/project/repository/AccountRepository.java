package ro.kronsoft.project.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ro.kronsoft.project.entities.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	Account getById(Long id);

	Optional<Account> findByUsername(String username);

	boolean existsByUsername(String username);

	@Query(value = "SELECT EXISTS(SELECT * FROM account WHERE id != :id AND username = :username)", nativeQuery = true)
	boolean existsByUsernameNotId(@Param("username") String username, @Param("id") Long id);

}

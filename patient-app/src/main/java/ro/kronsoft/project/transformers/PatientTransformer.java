package ro.kronsoft.project.transformers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.PatientDto;
import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.repository.PatientRepository;

@Component
public class PatientTransformer extends AbstractTransformer<Patient, PatientDto>{

	@Autowired
	private PatientRepository patientRepository;
	
	@Override
	public PatientDto toDto(Patient patient) {
		PatientDto dto = new PatientDto();
		BeanUtils.copyProperties(patient, dto);
		return dto;
	}

	@Override
	public Patient toEntity(PatientDto dto) {
		Patient patient = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, patient);
		return patient;
	}

	@Override
	protected Patient findOrCreateNew(Long id) {
		return id == null ? new Patient() : patientRepository.findById(id).orElseGet(Patient::new);
	}

}

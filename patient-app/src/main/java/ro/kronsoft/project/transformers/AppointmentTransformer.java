package ro.kronsoft.project.transformers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.AppointmentDto;
import ro.kronsoft.project.entities.Appointment;
import ro.kronsoft.project.repository.AppointmentRepository;
import ro.kronsoft.project.repository.PatientRepository;

@Component
public class AppointmentTransformer extends AbstractTransformer<Appointment, AppointmentDto> {

	@Autowired
	private AppointmentRepository appointmentRepository;
	
	@Autowired
	private PatientRepository patientRepository;

	@Override
	public AppointmentDto toDto(Appointment appointment) {
		AppointmentDto dto = new AppointmentDto();
		BeanUtils.copyProperties(appointment, dto);
		dto.setPatientId(appointment.getPatient().getId());
		return dto;
	}

	@Override
	public Appointment toEntity(AppointmentDto dto) {
		Appointment appointment = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, appointment);
		appointment.setPatient(patientRepository.getById(dto.getPatientId()));
		return appointment;
	}

	@Override
	protected Appointment findOrCreateNew(Long id) {
		return id == null ? new Appointment() : appointmentRepository.findById(id).orElseGet(Appointment::new);
	}

}

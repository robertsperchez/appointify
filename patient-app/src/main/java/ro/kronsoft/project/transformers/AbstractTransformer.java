package ro.kronsoft.project.transformers;

import ro.kronsoft.project.dto.BaseDto;
import ro.kronsoft.project.entities.BaseEntity;

public abstract class AbstractTransformer<T extends BaseEntity, x extends BaseDto> {
	
	public abstract x toDto(T entity);
	
	public abstract T toEntity(x dto);
	
	protected abstract T findOrCreateNew(Long id);
	
}

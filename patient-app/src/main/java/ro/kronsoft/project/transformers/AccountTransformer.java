package ro.kronsoft.project.transformers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.AccountDto;
import ro.kronsoft.project.entities.Account;
import ro.kronsoft.project.repository.AccountRepository;

@Component
public class AccountTransformer extends AbstractTransformer<Account, AccountDto> {

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public AccountDto toDto(Account account) {
		AccountDto dto = new AccountDto();
		BeanUtils.copyProperties(account, dto, "password");
		return dto;
	}

	@Override
	public Account toEntity(AccountDto dto) {
		Account account = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, account, "password");
		if (dto.getPassword() != null) {
			account.setPassword(dto.getPassword());
		}
		return account;
	}

	@Override
	protected Account findOrCreateNew(Long id) {
		return id == null ? new Account() : accountRepository.findById(id).orElseGet(Account::new);
	}

}

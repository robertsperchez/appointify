package ro.kronsoft.project.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.kronsoft.project.dto.AppointmentDto;
import ro.kronsoft.project.services.AppointmentService;
import ro.kronsoft.project.transformers.AppointmentTransformer;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private AppointmentTransformer appointmentTransformer;

	/**
	 * Returns a list of appointments dto's in a JSON format.
	 * 
	 * @return Returns a list of appointments.
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> getAppointments() {
		return appointmentService.findAllAppointments().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of appointments dto's in a JSON format, sorted by the date and hour ASC.
	 * 
	 * @return Returns a list of sorted appointments ASC.
	 */
	@GetMapping(value = "/sortByDateAsc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> sortAppointmentsByDateAndHourAsc() {
		return appointmentService.sortAppointmentsByDateAndHourAsc().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of appointments dto's in a JSON format, sorted by the date DESC.
	 * 
	 * @return Returns a list of sorted appointments DESC.
	 */
	@GetMapping(value = "/sortByDateDesc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> sortAppointmentsByDateDesc() {
		return appointmentService.sortAppointmentsByDateDesc().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of appointments dto's in a JSON format, sorted by the type.
	 * 
	 * @return Returns a list of sorted appointments by type.
	 */
	@GetMapping(value = "/sortByType", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> sortAppointmentsByType() {
		return appointmentService.sortAppointmentsByType().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of appointments dto's in a JSON format, sorted by the status.
	 * 
	 * @return Returns a list of sorted appointments by status.
	 */
	@GetMapping(value = "/sortByStatus", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> sortAppointmentsByStatus() {
		return appointmentService.sortAppointmentsByStatus().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of appointments dto's in a JSON format, sorted by the specialty.
	 * 
	 * @return Returns a list of sorted appointments by specialty.
	 */
	@GetMapping(value = "/sortBySpecialty", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> sortAppointmentsBySpecialty() {
		return appointmentService.sortAppointmentsBySpecialty().stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of appointments dto's in a JSON format from a specific
	 * patient.
	 * 
	 * @param id The id used to identify the patient.
	 * @return Returns a list of appointments.
	 */
	@GetMapping(value = "/patient/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> getAppointmentsByPatientId(@PathVariable Long id) {
		return appointmentService.getAppointmentsByPatientId(id).stream().map(appointmentTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns an appointment using its id.
	 * 
	 * @param id The id used to identify the appointment.
	 * @return Returns an AppointmentDto
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AppointmentDto getAppointmentById(@PathVariable Long id) {
		return appointmentTransformer.toDto(appointmentService.getAppointmentById(id));
	}

	/**
	 * Receives information about an appointment from the user, transforms the dto
	 * to an entity and then saves it in the database.
	 * 
	 * @param appointment AppointmentDto that is transformed into an entity and
	 *                    saved.
	 * @return Returns the created appointment.
	 */
	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public AppointmentDto createAppointment(@Validated @RequestBody AppointmentDto appointment) {
		return appointmentTransformer
				.toDto(appointmentService.saveAppointment(appointmentTransformer.toEntity(appointment)));
	}

	/**
	 * Updates an appointment using information saved in a AppointmentDto.
	 * 
	 * @param appointment AppointmentDto that saves information coming from the
	 *                    user.
	 */
	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAppointment(@Validated @RequestBody AppointmentDto appointment) {
		appointmentService.saveAppointment(appointmentTransformer.toEntity(appointment));
	}

	/**
	 * Deletes an appointment from the database.
	 * 
	 * @param id Used to identify the appointment being deleted.
	 */
	@DeleteMapping(value = "/{id}/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAppointment(@PathVariable Long id) {
		appointmentService.deleteAppointment(id);
	}

	/**
	 * Deletes all appointments in the database.
	 */
	@DeleteMapping(value = "/deleteAll")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllAppointments() {
		appointmentService.deleteAllAppointments();
	}
}

package ro.kronsoft.project.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.kronsoft.project.dto.AccountDto;
import ro.kronsoft.project.services.AccountService;
import ro.kronsoft.project.transformers.AccountTransformer;

@RestController
@RequestMapping("/account")
public class AccountController {
	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountTransformer accountTransformer;

	/**
	 * Returns a list of accounts dto's in a JSON format.
	 * 
	 * @return Returns a list of accounts.
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AccountDto> getAccounts() {
		return accountService.findAllAccounts().stream().map(accountTransformer::toDto).collect(Collectors.toList());
	}

	/**
	 * Returns an account using his id.
	 * 
	 * @param id The id used to identify the account.
	 * @return Returns an AccountDto
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AccountDto getAccountById(@PathVariable Long id) {
		return accountTransformer.toDto(accountService.getAccountById(id));
	}

	/**
	 * Returns an account using his username.
	 * 
	 * @param username The username used to identify the account.
	 * @return Returns an AccountDto
	 */
	@GetMapping(value = "/username/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AccountDto getAccountByUsername(@PathVariable String username) {
		return accountTransformer.toDto(accountService.findByUsername(username));
	}

	/**
	 * Receives information about an account from the user, transforms the dto to an
	 * entity and then saves it in the database.
	 * 
	 * @param account AccountDto that is transformed into an entity and saved.
	 * @return Returns the created account.
	 */
	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public AccountDto createAccount(@Validated @RequestBody AccountDto account) {
		return accountTransformer.toDto(accountService.createAccount(accountTransformer.toEntity(account)));
	}

	/**
	 * Updates an account using information saved in a AccountDto.
	 * 
	 * @param account AccountDto that saves information coming from the user.
	 */
	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAccount(@Validated @RequestBody AccountDto account) {
		accountService.updateAccount(accountTransformer.toEntity(account));
	}

	/**
	 * Deletes an account from the database.
	 * 
	 * @param id Used to identify the account being deleted.
	 */
	@DeleteMapping(value = "/{id}/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAccount(@PathVariable Long id) {
		accountService.deleteAccount(id);
	}

	/**
	 * Deletes all accounts in the database.
	 */
	@DeleteMapping(value = "/deleteAll")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllAccounts() {
		accountService.deleteAllAccounts();
	}
}

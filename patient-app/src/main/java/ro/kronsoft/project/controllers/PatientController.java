package ro.kronsoft.project.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.kronsoft.project.dto.PatientDto;
import ro.kronsoft.project.services.PatientService;
import ro.kronsoft.project.transformers.PatientTransformer;

@RestController
@RequestMapping("/patient")
public class PatientController {

	@Autowired
	private PatientService patientService;

	@Autowired
	private PatientTransformer patientTransformer;

	/**
	 * Returns a list of patients dto's in a JSON format.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> getPatients() {
		return patientService.findAllPatients().stream().map(patientTransformer::toDto).collect(Collectors.toList());
	}

	/**
	 * Returns a patient using his id.
	 * 
	 * @param id The id used to identify the patient.
	 * @return Returns a PatientDto
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientDto getPatientById(@PathVariable Long id) {
		return patientTransformer.toDto(patientService.getPatientById(id));
	}

	/**
	 * Returns a list of patients dto's in a JSON format sorted by their name ASC.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(value = "/sortByNameAsc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> sortPatientsByNameAsc() {
		return patientService.sortPatientsByNameAsc().stream().map(patientTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of patients dto's in a JSON format sorted by their name DESC.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(value = "/sortByNameDesc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> sortPatientsByNameDesc() {
		return patientService.sortPatientsByNameDesc().stream().map(patientTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of patients dto's in a JSON format sorted by their birthdate
	 * ASC.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(value = "/sortByBirthdateAsc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> sortPatientsByBirthdateAsc() {
		return patientService.sortPatientsByBirthdateAsc().stream().map(patientTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of patients dto's in a JSON format sorted by their birthdate
	 * DESC.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(value = "/sortByBirthdateDesc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> sortPatientsByBirthdateDesc() {
		return patientService.sortPatientsByBirthdateDesc().stream().map(patientTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Returns a list of patients dto's in a JSON format sorted by their sex type.
	 * 
	 * @return Returns a list of patients.
	 */
	@GetMapping(value = "/sortBySexType", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> sortPatientsBySexType() {
		return patientService.sortPatientsBySexType().stream().map(patientTransformer::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Receives information about a patient from the user, transforms the dto to an
	 * entity and then saves it in the database.
	 * 
	 * @param patient PatientDto that is transformed into an entity and saved.
	 * @return Returns the created patient.
	 */
	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public PatientDto createPatient(@Validated @RequestBody PatientDto patient) {
		return patientTransformer.toDto(patientService.savePatient(patientTransformer.toEntity(patient)));
	}

	/**
	 * Updates a patient using information saved in a PatientDto.
	 * 
	 * @param patient PatientDto that saves information coming from the user.
	 */
	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePatient(@Validated @RequestBody PatientDto patient) {
		patientService.savePatient(patientTransformer.toEntity(patient));
	}

	/**
	 * Deletes a patient from the database.
	 * 
	 * @param id Used to identify the patient being deleted.
	 */
	@DeleteMapping(value = "/{id}/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePatient(@PathVariable Long id) {
		patientService.deletePatient(id);
	}

	/**
	 * Deletes all patients in the database.
	 */
	@DeleteMapping(value = "/deleteAll")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllPatients() {
		patientService.deleteAllPatients();
	}
}

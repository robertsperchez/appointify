package ro.kronsoft.project.controllers;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ro.kronsoft.project.dto.ContactEmailDto;
import ro.kronsoft.project.services.ContactEmailService;

@RestController
public class ContactEmailController {

	@Autowired
	private ContactEmailService contactEmailService;
	
	@PostMapping(value = "/sendemail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ContactEmailDto sendMail(@RequestBody ContactEmailDto email) throws AddressException, MessagingException, IOException {
		contactEmailService.mailSender(email);
		return email;
	}

}

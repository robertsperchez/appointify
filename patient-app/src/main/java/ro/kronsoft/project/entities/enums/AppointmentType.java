package ro.kronsoft.project.entities.enums;

public enum AppointmentType {
	REGULAR,
	HOLIDAY,
	VACATION,
	GROUP;
}

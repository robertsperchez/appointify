package ro.kronsoft.project.entities;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ro.kronsoft.project.entities.enums.AppointmentStatus;
import ro.kronsoft.project.entities.enums.AppointmentType;
import ro.kronsoft.project.entities.enums.SpecialtyType;

@Entity
public class Appointment extends BaseEntity {

	private static final long serialVersionUID = -4252395705156674559L;

	@Column(name = "TYPE", length = 32, nullable = false)
	@Enumerated(EnumType.STRING)
	private AppointmentType appointmentType;

	@Column(name = "STATUS", length = 32, nullable = false)
	@Enumerated(EnumType.STRING)
	private AppointmentStatus appointmentStatus;

	@Column(name = "SPECIALTY", length = 32, nullable = false)
	@Enumerated(EnumType.STRING)
	private SpecialtyType specialtyType;

	@Column(name = "DATE", nullable = false)
	private LocalDate date;

	@Column(name = "START_TIME", nullable = false)
	private LocalTime startTime;

	@Column(name = "END_TIME", nullable = false)
	private LocalTime endTime;

	@Column(name = "DESCRIPTION", nullable = true, length = 512)
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "patient_id")
	private Patient patient;
	
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Appointment(AppointmentType appointmentType, AppointmentStatus appointmentStatus,
			SpecialtyType specialtyType, LocalDate date, LocalTime startTime, LocalTime endTime, String description,
			Patient patient) {
		super();
		this.appointmentType = appointmentType;
		this.appointmentStatus = appointmentStatus;
		this.specialtyType = specialtyType;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.patient = patient;
	}

	public Appointment() {
		super();
	}

	//to change it once it's done
	@Override
	public String toString() {
		return "Appointment [appointmentType=" + appointmentType + ", appointmentStatus=" + appointmentStatus
				+ ", specialtyType=" + specialtyType + ", date=" + date + ", startTime=" + startTime + ", endTime="
				+ endTime + ", description=" + description + "]";
	}

}

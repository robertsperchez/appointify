package ro.kronsoft.project.entities.enums;

public enum SexTypeEnum {
	MALE, 
	FEMALE, 
	UNDEFINED;
}

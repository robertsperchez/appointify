package ro.kronsoft.project.entities.enums;

public enum AccountStatus {
	APPROVED,
	PENDING
}

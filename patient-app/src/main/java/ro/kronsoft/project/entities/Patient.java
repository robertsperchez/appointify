package ro.kronsoft.project.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

import ro.kronsoft.project.entities.enums.SexTypeEnum;

@Entity
public class Patient extends BaseEntity {

	private static final long serialVersionUID = -4883763721450990096L;
	
	@Column(name = "FIRST_NAME", nullable = false, length = 64)
	@Length(min = 0, max = 64)
	private String firstName;
	
	@Column(name = "LAST_NAME", nullable = false, length = 64)
	@Length(min = 0, max = 64)
	private String lastName;
	
	@Column(name = "BIRTHDATE", nullable = false)
	private LocalDate birthDate;
	
	@Column(name = "CNP", nullable = false, length = 13)
	private String personalNumericCode;
	
	@Column(name = "SEX", length = 32, nullable = false)
	@Enumerated(EnumType.STRING)
	private SexTypeEnum sexType;
	
	@Column(name = "CITY", nullable = true, length = 64)
	private String city;
	
	@Column(name = "COUNTRY", nullable = true, length = 64)
	private String country;
	
	@Column(name = "EMAIL", nullable = false, length = 64, unique = true)
	@Email
	private String email;
	
	@Column(name = "PHONE_NUMBER", nullable = false, length = 32)
	private String phoneNumber;
	
	@Column(name = "ADDRESS", nullable = true, length = 128)
	private String address;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
	private Set<Appointment> appointments = new HashSet<>();

	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getPersonalNumericCode() {
		return personalNumericCode;
	}

	public void setPersonalNumericCode(String personalNumericCode) {
		this.personalNumericCode = personalNumericCode;
	}

	public SexTypeEnum getSexType() {
		return sexType;
	}

	public void setSexType(SexTypeEnum sexType) {
		this.sexType = sexType;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Patient(@Length(min = 0, max = 64) String firstName, @Length(min = 0, max = 64) String lastName,
			LocalDate birthDate, String personalNumericCode, SexTypeEnum sexType, String city, String country,
			@Email String email, String phoneNumber, String address, Set<Appointment> appointments) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.personalNumericCode = personalNumericCode;
		this.sexType = sexType;
		this.city = city;
		this.country = country;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.appointments = appointments;
	}

	public Patient(@Length(min = 0, max = 64) String firstName, @Length(min = 0, max = 64) String lastName,
			LocalDate birthDate, String personalNumericCode, SexTypeEnum sexType, String city, String country,
			@Email String email, String phoneNumber, String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.personalNumericCode = personalNumericCode;
		this.sexType = sexType;
		this.city = city;
		this.country = country;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public Patient() {
		super();
	}

	//to change it once it's done
	@Override
	public String toString() {
		return "Patient [firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate
				+ ", personalNumericCode=" + personalNumericCode + ", sexType=" + sexType + ", city=" + city
				+ ", country=" + country + ", email=" + email + ", phoneNumber=" + phoneNumber + ", address=" + address
				+ "]";
	}

}

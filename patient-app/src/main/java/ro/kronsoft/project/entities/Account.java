package ro.kronsoft.project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import ro.kronsoft.project.entities.enums.AccountStatus;

@Entity
public class Account extends BaseEntity {

	private static final long serialVersionUID = 303493182677303604L;

	@Column(name = "USERNAME", nullable = false, length = 32, unique = true)
	private String username;

	@Column(name = "PASSWORD", nullable = false, length = 64)
	private String password;

	@Column(name = "ACCOUNT_STATUS", nullable = false)
	@Enumerated(EnumType.STRING)
	private AccountStatus authority;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AccountStatus getAccountStatus() {
		return authority;
	}

	public void setAccountStatus(AccountStatus authority) {
		this.authority = authority;
	}

	public Account(String username, String password, AccountStatus authority) {
		super();
		this.username = username;
		this.password = password;
		this.authority = authority;
	}

	public Account() {
		super();
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", accountStatus=" + authority + "]";
	}

}

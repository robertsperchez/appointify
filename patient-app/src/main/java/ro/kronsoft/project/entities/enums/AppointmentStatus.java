package ro.kronsoft.project.entities.enums;

public enum AppointmentStatus {
	CREATED,
	PLANNED,
	CONFIRMED,
	CLOSED,
	CANCELLED;
}

package ro.kronsoft.project.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.AccountDto;
import ro.kronsoft.project.repository.AccountRepository;

@Component
public class AccountValidator implements ConstraintValidator<AccountValidation, AccountDto> {

	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public boolean isValid(AccountDto value, ConstraintValidatorContext context) {
		String username = value.getUsername();
		Long id = value.getId();

		boolean isValidUsername = !(id == null ? accountRepository.existsByUsername(username)
				: accountRepository.existsByUsernameNotId(username, id));
		
		if (!isValidUsername) {
			context.buildConstraintViolationWithTemplate(
					String.format("The account with the username=%s already exists!", username))
					.addPropertyNode("username").addConstraintViolation();
			return false;
		}

		return true;
	}

}

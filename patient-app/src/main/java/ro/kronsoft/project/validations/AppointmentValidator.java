package ro.kronsoft.project.validations;

import java.time.LocalTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.AppointmentDto;
import ro.kronsoft.project.repository.PatientRepository;

@Component
public class AppointmentValidator implements ConstraintValidator<AppointmentValidation, AppointmentDto> {

	@Autowired
	private PatientRepository patientRepository;

	@Override
	public boolean isValid(AppointmentDto value, ConstraintValidatorContext context) {
		LocalTime appStartTime = value.getStartTime();
		LocalTime appEndTime = value.getEndTime();
		Long patientId = value.getPatientId();

		boolean isValidTime = !appStartTime.isAfter(appEndTime);
		boolean isValidPatient = patientRepository.existsById(patientId);

		if (!isValidTime) {
			context.buildConstraintViolationWithTemplate(
					String.format("The start time can't be after the end time!", appStartTime))
					.addPropertyNode("startTime").addConstraintViolation();
			return false;
		}
		if (!isValidPatient) {
			context.buildConstraintViolationWithTemplate(
					String.format("The patient does not exist!", patientId))
					.addPropertyNode("patientId").addConstraintViolation();
			return false;
		}

		return true;
	}

}

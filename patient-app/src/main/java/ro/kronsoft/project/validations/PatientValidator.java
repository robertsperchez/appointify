package ro.kronsoft.project.validations;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.project.dto.PatientDto;
import ro.kronsoft.project.repository.PatientRepository;

@Component
public class PatientValidator implements ConstraintValidator<PatientValidation, PatientDto> {

	@Autowired
	private PatientRepository patientRepository;

	@Override
	public boolean isValid(PatientDto value, ConstraintValidatorContext context) {
		Long patientId = value.getId();
		String patientEmail = value.getEmail();
		LocalDate patientBirthdate = value.getBirthDate();

		boolean isValidEmail = !(patientId == null ? patientRepository.existsByEmail(patientEmail)
				: patientRepository.existsByEmailNotId(patientEmail, patientId));

		boolean isValidBirthdate = !patientBirthdate.isAfter(LocalDate.now());

		if (!isValidEmail) {
			context.buildConstraintViolationWithTemplate(
					String.format("The patient with the email=%s already exists!", patientEmail))
					.addPropertyNode("email").addConstraintViolation();
			return false;
		}

		if (!isValidBirthdate) {
			context.buildConstraintViolationWithTemplate(
					String.format("The chosen birthdate is in the future!", patientBirthdate))
					.addPropertyNode("birthDate").addConstraintViolation();
			return false;
		}

		return true;
	}

}

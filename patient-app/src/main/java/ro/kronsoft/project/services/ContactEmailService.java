package ro.kronsoft.project.services;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import ro.kronsoft.project.dto.ContactEmailDto;

@Service
public class ContactEmailService {

	public void mailSender(ContactEmailDto email) throws AddressException, MessagingException, IOException {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("patientapp.kronsoft@gmail.com", "Internship1");
			}
		});

		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("patientapp.kronsoft@gmail.com", false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("patientapp.kronsoft@gmail.com"));
		msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(email.getEmail()));
		msg.setSubject(email.getName() + ": Inquiry");
		msg.setContent("Name: " + email.getName() + "\nPhone Number: " + email.getPhoneNumber() + "\nCompany: "
				+ email.getCompany() + "\nMessage: " + email.getMessage(), "text/html");
		Transport.send(msg);
	}

}

package ro.kronsoft.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.kronsoft.project.entities.Account;
import ro.kronsoft.project.entities.enums.AccountStatus;
import ro.kronsoft.project.repository.AccountRepository;

@Service
@Transactional
public class AccountService implements UserDetailsService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("The username does not exist!"));

		return org.springframework.security.core.userdetails.User.withUsername(account.getUsername())
				.password(account.getPassword())
				.authorities(String.format("ROLE_%s", account.getAccountStatus().toString())).build();
	}

	public Account findByUsername(String username) {
		return accountRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("The username does not exist!"));
	}

	public Account getAccountById(Long id) {
		return accountRepository.getById(id);
	}

	public List<Account> findAllAccounts() {
		return accountRepository.findAll();
	}

	public Account saveAccount(Account account) {
		return accountRepository.save(account);
	}

	public Account createAccount(Account account) {
		account.setPassword(passwordEncoder.encode(account.getPassword()));
		account.setAccountStatus(AccountStatus.PENDING);
		return saveAccount(account);
	}

	public void updateAccount(Account account) {
		String password = account.getPassword();
		String savedPassword = accountRepository.getById(account.getId()).getPassword();
		if (!password.equals(savedPassword) || !passwordEncoder.matches(password, savedPassword)) {
			account.setPassword(passwordEncoder.encode(password));
		}
		saveAccount(account);
	}

	public void deleteAccount(Long id) {
		accountRepository.deleteById(id);
	}

	public void deleteAllAccounts() {
		accountRepository.deleteAll();
	}

}

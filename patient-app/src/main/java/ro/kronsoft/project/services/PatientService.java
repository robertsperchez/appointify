package ro.kronsoft.project.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.repository.PatientRepository;

@Service
@Transactional
public class PatientService {
	
	@Autowired
	private PatientRepository patientRepository;
	
	public Patient getPatientById(Long id) {
		return patientRepository.getById(id);
	}
	
	public List<Patient> sortPatientsByNameAsc() {
		return patientRepository.sortByNameAsc();
	}
	
	public List<Patient> sortPatientsByNameDesc() {
		return patientRepository.sortByNameDesc();
	}
	
	public List<Patient> sortPatientsByBirthdateAsc() {
		return patientRepository.sortByBirthdateAsc();
	}
	
	public List<Patient> sortPatientsByBirthdateDesc() {
		return patientRepository.sortByBirthdateDesc();
	}
	
	public List<Patient> sortPatientsBySexType() {
		return patientRepository.sortBySexType();
	}
	
	public List<Patient> findAllPatients() {
		return patientRepository.findAll();
	}

	public Patient savePatient(Patient patient) {
		return patientRepository.save(patient);
	}

	public void deletePatient(Long id) {
		patientRepository.deleteById(id);
	}

	public void deleteAllPatients() {
		patientRepository.deleteAll();
	}

}

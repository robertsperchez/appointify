package ro.kronsoft.project.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.kronsoft.project.entities.Appointment;
import ro.kronsoft.project.repository.AppointmentRepository;

@Service
@Transactional
public class AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;
	
	public Appointment getAppointmentById(Long id) {
		return appointmentRepository.getById(id);
	}
	
	public List<Appointment> findAllAppointments() {
		return appointmentRepository.findAll();
	}
	
	public List<Appointment> sortAppointmentsByDateAndHourAsc() {
		return appointmentRepository.sortByDateAndHourAsc();
	}
	
	public List<Appointment> sortAppointmentsByDateDesc() {
		return appointmentRepository.sortByDateDesc();
	}
	
	public List<Appointment> sortAppointmentsByType() {
		return appointmentRepository.sortByType();
	}

	public List<Appointment> sortAppointmentsByStatus() {
		return appointmentRepository.sortByStatus();
	}
	
	public List<Appointment> sortAppointmentsBySpecialty() {
		return appointmentRepository.sortBySpecialty();
	}
	
	public Appointment saveAppointment(Appointment appointment) {
		return appointmentRepository.save(appointment);
	}

	public void deleteAppointment(Long id) {
		appointmentRepository.deleteById(id);
	}

	public void deleteAllAppointments() {
		appointmentRepository.deleteAll();
	}
	
	public List<Appointment> getAppointmentsByPatientId(Long id) {
		return appointmentRepository.getByPatientId(id);
	}
	
}

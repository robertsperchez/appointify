package ro.kronsoft.tests.dto;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ro.kronsoft.project.ApplicationMain;
import ro.kronsoft.project.dto.AccountDto;
import ro.kronsoft.project.dto.AppointmentDto;
import ro.kronsoft.project.dto.PatientDto;
import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.entities.enums.AccountStatus;
import ro.kronsoft.project.entities.enums.AppointmentStatus;
import ro.kronsoft.project.entities.enums.AppointmentType;
import ro.kronsoft.project.entities.enums.SexTypeEnum;
import ro.kronsoft.project.entities.enums.SpecialtyType;

@SpringBootTest(classes = ApplicationMain.class)
public class DtoTest {

	@Test
	public void testPatientDto() {
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		PatientDto patient = new PatientDto("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");

		assertNotNull(patient);
		assertTrue(patient.getClass() == PatientDto.class);
	}

	@Test
	public void testAppointmentDto() {
		LocalDate date = LocalDate.of(2021, 1, 1);
		LocalTime startTime = LocalTime.of(12, 0);
		LocalTime endTime = LocalTime.of(12, 30);
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		Patient patient = new Patient("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");
		patient.setId(1l);
		AppointmentDto appointment = new AppointmentDto(AppointmentType.REGULAR, AppointmentStatus.CREATED,
				SpecialtyType.CARDIOLOGY, date, startTime, endTime, "Description", patient.getId());

		assertNotNull(appointment);
		assertNotNull(appointment.getPatientId());
		assertTrue(appointment.getClass() == AppointmentDto.class);
		assertTrue(appointment.getPatientId() == patient.getId());
	}

	@Test
	public void testAccountDto() {
		AccountDto account = new AccountDto("username", "password", AccountStatus.APPROVED);

		assertNotNull(account);
		assertTrue(account.getClass() == AccountDto.class);
	}
	
}

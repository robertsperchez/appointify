package ro.kronsoft.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ro.kronsoft.project.ApplicationMain;
import ro.kronsoft.project.dto.AppointmentDto;
import ro.kronsoft.project.entities.Appointment;
import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.entities.enums.AppointmentStatus;
import ro.kronsoft.project.entities.enums.AppointmentType;
import ro.kronsoft.project.entities.enums.SexTypeEnum;
import ro.kronsoft.project.entities.enums.SpecialtyType;
import ro.kronsoft.project.transformers.AppointmentTransformer;

@SpringBootTest(classes = ApplicationMain.class)
public class AppointmentTransformerTests {
	
	@Autowired
	private AppointmentTransformer appointmentTransformer;
	
	@Test
	private void appointmentToDtoTest() {
		LocalDate date = LocalDate.of(2021, 1, 1);
		LocalTime startTime = LocalTime.of(12, 0);
		LocalTime endTime = LocalTime.of(12, 30);
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		Patient patient = new Patient("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");
		patient.setId(1l);
		Appointment appointment = new Appointment(AppointmentType.REGULAR, AppointmentStatus.CREATED,
				SpecialtyType.CARDIOLOGY, date, startTime, endTime, "Description", patient);
		AppointmentDto appointmentDto = appointmentTransformer.toDto(appointment);
		
		assertNotNull(appointment);
		assertNotNull(appointmentDto);
		assertEquals(appointment.getPatient().getId(), appointmentDto.getPatientId());
		assertTrue(appointment.getAppointmentStatus() == appointmentDto.getAppointmentStatus());
		assertTrue(appointment.getStartTime() == appointmentDto.getStartTime());
		assertTrue(appointment.getDate() == appointmentDto.getDate());
	}

}

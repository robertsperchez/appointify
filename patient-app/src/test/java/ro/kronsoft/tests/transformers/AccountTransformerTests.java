package ro.kronsoft.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ro.kronsoft.project.ApplicationMain;
import ro.kronsoft.project.dto.AccountDto;
import ro.kronsoft.project.entities.Account;
import ro.kronsoft.project.entities.enums.AccountStatus;
import ro.kronsoft.project.transformers.AccountTransformer;

@SpringBootTest(classes = ApplicationMain.class)
public class AccountTransformerTests {

	@Autowired
	private AccountTransformer accountTransformer;
	
	@Test
	private void testAccountToDto() {
		Account account = new Account("username", "password", AccountStatus.APPROVED);
		AccountDto accountDto = accountTransformer.toDto(account);

		assertNotNull(accountDto);
		assertTrue(account.getAccountStatus() == accountDto.getAccountStatus());
		assertTrue(account.getUsername() == accountDto.getUsername());
		assertTrue(account.getPassword() == accountDto.getPassword());
	}
}

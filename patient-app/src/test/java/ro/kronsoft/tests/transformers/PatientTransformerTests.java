package ro.kronsoft.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ro.kronsoft.project.ApplicationMain;
import ro.kronsoft.project.dto.PatientDto;
import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.entities.enums.SexTypeEnum;
import ro.kronsoft.project.transformers.PatientTransformer;

@SpringBootTest(classes = ApplicationMain.class)
public class PatientTransformerTests {

	@Autowired
	private PatientTransformer patientTransformer;
	
	@Test
	public void testPatientToDto() {
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		Patient patient = new Patient("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");
		PatientDto patientDto = patientTransformer.toDto(patient);
		
		assertNotNull(patientDto);
		assertNotNull(patient);
		assertTrue(patient.getBirthDate() == patientDto.getBirthDate());
		assertTrue(patient.getSexType() == patientDto.getSexType());
		assertTrue(patient.getFirstName() == patientDto.getFirstName());
	}
}

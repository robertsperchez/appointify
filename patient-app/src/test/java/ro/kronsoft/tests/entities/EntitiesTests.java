package ro.kronsoft.tests.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ro.kronsoft.project.ApplicationMain;
import ro.kronsoft.project.entities.Account;
import ro.kronsoft.project.entities.Appointment;
import ro.kronsoft.project.entities.Patient;
import ro.kronsoft.project.entities.enums.AccountStatus;
import ro.kronsoft.project.entities.enums.AppointmentStatus;
import ro.kronsoft.project.entities.enums.AppointmentType;
import ro.kronsoft.project.entities.enums.SexTypeEnum;
import ro.kronsoft.project.entities.enums.SpecialtyType;

@SpringBootTest(classes = ApplicationMain.class)
public class EntitiesTests {

	@Test
	public void testPatientObject() {
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		Patient patient = new Patient("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");

		assertNotNull(patient);
		assertTrue(patient.getClass() == Patient.class);
	}

	@Test
	public void testAppointmentObject() {
		LocalDate date = LocalDate.of(2021, 1, 1);
		LocalTime startTime = LocalTime.of(12, 0);
		LocalTime endTime = LocalTime.of(12, 30);
		LocalDate birthdate = LocalDate.of(2000, 1, 1);
		Patient patient = new Patient("Prenume", "Nume", birthdate, "5010100000001", SexTypeEnum.MALE, "Oras", "Tara",
				"email@email.com", "0751000111", "Adresa");
		Appointment appointment = new Appointment(AppointmentType.REGULAR, AppointmentStatus.CREATED,
				SpecialtyType.CARDIOLOGY, date, startTime, endTime, "Description", patient);

		assertNotNull(appointment);
		assertNotNull(appointment.getPatient());
		assertTrue(appointment.getClass() == Appointment.class);
		assertTrue(appointment.getPatient().getClass() == Patient.class);
		assertEquals(appointment.getPatient(), patient);
	}

	@Test
	public void testAccountObject() {
		Account account = new Account("username", "password", AccountStatus.APPROVED);

		assertNotNull(account);
		assertTrue(account.getClass() == Account.class);
	}

}
